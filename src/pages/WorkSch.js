import React from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Template from "../components/Template";

// กำหนดการใช้งาน localizer โดยใช้ moment.js
const localizer = momentLocalizer(moment);

function WorkSch() {
  // สร้างข้อมูลเหตุการณ์ตัวอย่าง
  const events = [
    {
      title: "เริ่มต้นงาน",
      start: new Date(),
      end: new Date(),
    },
    {
      title: "สิ้นสุดงาน",
      start: new Date(),
      end: new Date(),
    },
  ];

  return (
    <>
      <Template>
        <div class="container">
          <div class="row">
            <div class="col-3"></div>
            <div class="col-9">
              <div style={{ height: "500px" }}>
                <Calendar
                  localizer={localizer}
                  events={events} // ส่งข้อมูลเหตุการณ์ไปยังปฏิทิน
                  startAccessor="start"
                  endAccessor="end"
                  style={{ marginBottom: "20px" }}
                />
              </div>
            </div>
          </div>
        </div>
      </Template>
    </>
  );
}

export default WorkSch;
