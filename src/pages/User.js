import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import Template from "../components/Template";
import Swal from "sweetalert2";
import config from "../config";
import axios from "axios";

function User() {
  const [user, setUser] = useState({});
  const [users, setUsers] = useState([]);
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [prefix, setPrefix] = useState({});
  const [prefixs, setPrefixs] = useState([]);
  const [position, setPosition] = useState({});
  const [positions, setPositions] = useState([]);

  useEffect(() => {
    fetchData();
    fetchDataPrefix();
    fetchDataPosition();
  }, []);

  const fetchData = async () => {
    try {
      await axios
        .get(config.api_path + "/user/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setUsers(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataPrefix = async () => {
    try {
      await axios
        .get(config.api_path + "/prefix/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setPrefixs(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataPosition = async () => {
    try {
        await axios
          .get(config.api_path + "/position/list", config.headers())
          .then((res) => {
            if (res.data.message === "success") {
              setPositions(res.data.results);
            }
          })
          .catch((err) => {
            throw err.response.data;
          });
      } catch (e) {
        Swal.fire({
          title: "error",
          text: e.message,
          icon: "error",
        });
      }
    };

  const handleSave = async () => {
    try {
      let url = "/user/insert";

      if (user.id !== undefined) {
        url = "/user/edit";
      }

      await axios
        .post(config.api_path + url, user, config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            Swal.fire({
              title: "บันทึกข้อมูล",
              text: "บันทึกข้อมูลเข้าระบบแล้ว",
              icon: "success",
              timer: 2000,
            });

            handleClose();
            fetchData();
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleClose = () => {
    const btns = document.getElementsByClassName("btnClose");
    for (let i = 0; i < btns.length; i++) {
      btns[i].click();
    }
  };

  const clearForm = () => {
    setUser({
      id: undefined,
      fname: "",
      lname: "",
      username: "",
      email: "",
      level: "1",
      status: "1",
      prefixId: "",
      positionId: ""
    });
    setPassword("");
    setPasswordConfirm("");
  };

  const changePassword = (item) => {
    setPassword(item);
    comparePassword();
  };

  const changePasswordConfirm = (item) => {
    setPasswordConfirm(item);
    comparePassword();
  };

  const comparePassword = () => {
    if (password.length > 0 && passwordConfirm.length > 0) {
      if (password != passwordConfirm) {
        Swal.fire({
          title: "ตรวจสอบการกรอกรหัสผ่าน",
          text: "โปรดกรอกรหัสผ่าน ให้ตรงกัน",
          icon: "error",
        });
      } else {
        setUser({
          ...user,
          password: password,
        });
      }
    }
  };

  const handleDelete = (item) => {
    try {
      Swal.fire({
        title: "ยืนยันการลบข้อมูล",
        text: "คุณต้องการลบข้อมูล ผู้ใช้งานใช่หรือไม่",
        icon: "question",
        showCancelButton: true,
        showConfirmButton: true,
      }).then(async (res) => {
        if (res.isConfirmed) {
          await axios
            .delete(
              config.api_path + "/user/delete/" + item.id,
              config.headers()
            )
            .then((res) => {
              if (res.data.message === "success") {
                Swal.fire({
                  title: "ลบข้อมูลแล้ว",
                  text: "ระบบได้ทำการลบข้อมูลเรียบร้อยแล้ว",
                  icon: "success",
                  timer: 2000,
                });

                fetchData();
              }
            })
            .catch((err) => {
              throw err.response.data;
            });
        }
      });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  return (
    <>
      <Template>
        <div className="card">
          <div className="card-header">
            <div className="card-title">ผู้ใช้งานระบบ</div>
          </div>
          <div className="card-body">
            <button
              onClick={clearForm}
              data-toggle="modal"
              data-target="#modalUser"
              className="btn btn-primary"
            >
              <i className="fa fa-plus me-2"></i>
              เพิ่มรายการ
            </button>

            <table className="mt-3 table table-bordered table-striped">
              <thead>
                <tr>
                  <th>username</th>
                  <th>ชื่อ - นามสกุล</th>
                  <th>ตำแหน่ง</th>
                  <th>กลุ่มผู้ใช้งาน</th>
                  <th>สถานะ</th>
                  <th width="150px"></th>
                </tr>
              </thead>
              <tbody>
                {users.length > 0
                  ? users.map((item) => (
                      <tr>
                        <td>{item.username}</td>
                        <td>
                        {prefixs.length > 0 ? (
                                <option value={item.prefixId}>
                                    {prefixs.find(prefix => prefix.id === item.prefixId)?.name}{item.fname} {item.lname}
                                </option>
                            ) : ''}                  
                        </td>
                        <td>
                        {positions.length > 0 ? (
                                <option value={item.positionId}>
                                    {positions.find(position => position.id === item.positionId)?.name}
                                </option>
                            ) : ''}   
                        </td>
                        <td>{item.level}</td>
                        <td>{item.status}</td>
                        <td className="text-center">
                          <button
                            onClick={(e) => setUser(item)}
                            data-toggle="modal"
                            data-target="#modalUser"
                            className="btn btn-info me-2"
                          >
                            <i className="fa fa-pencil"></i>
                          </button>
                          <button
                            onClick={(e) => handleDelete(item)}
                            className="btn btn-danger"
                          >
                            <i className="fa fa-times"></i>
                          </button>
                        </td>
                      </tr>
                    ))
                  : ""}
              </tbody>
            </table>
          </div>
        </div>
      </Template>

      <Modal id="modalUser" title="ผู้ใช้งานระบบ" modalSize="modal-lg">
        <div>
          <label>คำนำ</label>
          <select
            value={user.prefixId}
            onChange={(e) => setUser({ ...user, prefixId: e.target.value })}
            className="form-control"
          >
            {prefixs.length > 0
              ? prefixs.map((item) => (
                  <option value={item.id}>{item.name}</option>
                ))
              : ""}
          </select>

          <label>ชื่อ</label>
          <input
            value={user.fname}
            onChange={(e) => setUser({ ...user, fname: e.target.value })}
            className="form-control"
          />
          <label>นามสกุล</label>
          <input
            value={user.lname}
            onChange={(e) => setUser({ ...user, lname: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3">
          <label>ตำแหน่ง</label>
          <select
            value={user.positionId}
            onChange={(e) => setUser({ ...user, positionId: e.target.value })}
            className="form-control"
          >
            {positions.length > 0
              ? positions.map((item) => (
                  <option value={item.id}>{item.name}</option>
                ))
              : ""}
          </select>
        </div>
        <div className="mt-3">
          <label>username</label>
          <input
            value={user.username}
            onChange={(e) => setUser({ ...user, username: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3">
          <label>อีเมล์</label>
          <input
            value={user.email}
            onChange={(e) => setUser({ ...user, email: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3">
          <label>password</label>
          <input
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onBlur={(e) => changePassword(e.target.value)}
            type="password"
            className="form-control"
          />
        </div>
        <div className="mt-3">
          <label>ยืนยัน password</label>
          <input
            value={passwordConfirm}
            onChange={(e) => setPasswordConfirm(e.target.value)}
            onBlur={(e) => changePasswordConfirm(e.target.value)}
            type="password"
            className="form-control"
          />
        </div>
        <div className="mt-3">
          <label>กลุ่มผู้ใช้งาน</label>
          <select
            value={user.level}
            onChange={(e) => setUser({ ...user, level: e.target.value })}
            className="form-control"
          >
            <option value="1">เจ้าหน้าที่</option>
            <option value="2">ผู้พิพากษาสมทบ</option>
            <option value="3">ผู้บริหาร</option>
            <option value="9">Admin</option>
          </select>
        </div>
        <div className="mt-3">
          <label>สถานะการทำงาน</label>
          <select
            value={user.status}
            onChange={(e) => setUser({ ...user, status: e.target.value })}
            className="form-control"
          >
            <option value="1">ทำงานที่ศาล</option>
            <option value="0">ไม่ได้ทำงานแล้ว</option>
          </select>
        </div>
        <div className="mt-3">
          <button onClick={handleSave} className="btn btn-primary">
            <i className="fa fa-check me-2"></i>
            Save
          </button>
        </div>
      </Modal>
    </>
  );
}

export default User;
