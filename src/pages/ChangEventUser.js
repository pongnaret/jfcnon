import React, { useState, useEffect } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
//import interactionPlugin,{ Draggable } from "@fullcalendar/interaction"; // needed for dayClick
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin, { Draggable } from "@fullcalendar/interaction";
import Template from "../components/Template";
import { Button, Modal } from "antd";
import Swal from "sweetalert2";
import config from "../config";
import axios from "axios";
//import Item from "antd/es/list/Item";
import moment from "moment";

function ChangEventUser() {
  const [isModalOpenDetail, setIsModalOpenDetail] = useState(false);

  const [eventusers, setEventUsers] = useState([]);
  const [datalatest, setDatalatest] = useState([]);
  
  const [events, setEvents] = useState([]);
  const [eventuserdetail, setEventUserDetail] = useState([]);
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState();
  const [changeventuser, setChangEventUser] = useState();

  useEffect(() => {
    fetchData();
    fetchDataEvent();
    fetchDataUser();
    fetchDatalatest();
   
  }, []);

  const fetchData = async () => {
    try {
      await axios
        .get(config.api_path + "/changeventuser/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEventUsers(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataEvent = async () => {
    try {
      await axios
        .get(config.api_path + "/event/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEvents(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataUser = async () => {
    try {
      await axios
        .get(config.api_path + "/user/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setUsers(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };
  
  const fetchDatalatest = async () => {
    try {
      await axios
        .get(config.api_path + "/changeventuser/latest", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setDatalatest(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };


  
  const handleClick = async (info) => {
    //คลิกดูรายละเอียดในปฏิทิน

    const eventuser = {
      id: info.event._def.publicId,
    };
    //console.log(eventuser.id)
    try {
      await axios
        .get(
          config.api_path + "/eventuser/userdetail/" + eventuser.id,
          config.headers()
        )
        .then((res) => {
          if (res.data.message === "success") {
            setEventUserDetail(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }

    showModalDetail();
  };

  const showModalDetail = () => {
    //console.log(info)
    setIsModalOpenDetail(true);
  };

  const handleCancelDetail = () => {
    setIsModalOpenDetail(false);
  };

  const handleSave = (eventuserdetail, changeventuser, datalatest) => {
    if (changeventuser && changeventuser.ReceiveUserId) {
      const today = new Date();
      const currentDate = today.toISOString().split("T")[0];

      

      if (eventuserdetail.length > 0) {
        const changeventuser1 = {
          start: eventuserdetail[0].start,
          dateChang: currentDate,
          end: eventuserdetail[0].end,
          allDay: eventuserdetail[0].allDay,
          color: eventuserdetail[0].color,
          eventUserId: eventuserdetail[0].id,
          eventId: eventuserdetail[0].eventId,
          giveUserId: eventuserdetail[0].userId, 
          giveName: eventuserdetail[0].user.fname+' '+eventuserdetail[0].user.lname,           
          status: 1,
          approve: 9,   //9 รออนุมัติ
          ReceiveUserId: changeventuser.ReceiveUserId,
          ReceiveName: changeventuser.fname+' '+changeventuser.lname,
          fname: changeventuser.fname,
          lname: changeventuser.lname,
          ChangID : datalatest.id

        };

        console.log(changeventuser1);
         handleSaveToChang(changeventuser1);

         handleEditEventUser(changeventuser1);
         handleAddChangEventUser(changeventuser1);
         handleCancelDetail();
         fetchData();
      } else {
        console.log("ไม่มีข้อมูลกิจกรรมที่เลือก");
      }
    } else {
      Swal.fire({
        title: "Error",
        text: "ไม่ได้เลือกผู้มอบหมายให้ปฏิบัติหน้าที่แทน",
        icon: "error",
      });
    }   

    // console.log(changeventuser);
  };

  const handleSaveToChang = async (info) => {
    //console.log(info)
    try {
      let url = "/changeventuser/insert";

      // ถ้า eventuser.id มีค่า (ไม่เท่ากับ undefined) ให้เปลี่ยน url เป็น "/changeventuser/edit"
      if (changeventuser.id !== undefined) {
        url = "/changeventuser/edit";
      }

      // ส่งข้อมูลไปยังเซิร์ฟเวอร์โดยใช้ axios.post
      const response = await axios.post(
        config.api_path + url,
        info,
        config.headers()
      );

      // ตรวจสอบว่าการส่งข้อมูลสำเร็จหรือไม่
      if (response.data.message === "success") {
        // แสดงการแจ้งเตือนเมื่อสำเร็จ
        Swal.fire({
          title: "บันทึกข้อมูล",
          text: "บันทึกข้อมูลเข้าระบบแล้ว",
          icon: "success",
          timer: 2000,
        });
      } else {
        // แสดงข้อผิดพลาดหากไม่สำเร็จ
        Swal.fire({
          title: "error",
          text: response.data.message,
          icon: "error",
        });
      }
    } catch (error) {
      // แสดงข้อผิดพลาดหากมีข้อผิดพลาดในการส่งข้อมูล
      Swal.fire({
        title: "error",
        text: error.message,
        icon: "error",
      });
    }
  };

  const handleEditEventUser = async (info) => {
    const changeventuser2 = {
      id: info.eventUserId,
      //start: eventuserdetail[0].start,
      color: "#E22AF7",
      status: 9,
    };

    try {
      let url = "/eventuser/edit";

      await axios.post(
        config.api_path + url,
        changeventuser2,
        config.headers()
      );

      
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };



  const handleAddChangEventUser = async (info) => {
    
    console.log(info)
    const changeventuser3 = {
      title: "รออนุมัติ "+''+info.fname+' '+info.lname,
      color: "#F8031D ",
      userId: info.ReceiveUserId,
      start: info.start,
      end: info.end,
      allDay: 1,
      eventId: info.eventId,
      status: 2, // 1 = ผู้มาปฏิบัติงาน , 2 รออนุมัติ , 9 ให้คนอื่นมาทำแล้ว
      changID: info.ChangID + 1,  
    };
    

    try {
      let url = "/eventuser/insert";

      await axios.post(config.api_path + url, changeventuser3, config.headers());
      
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  
  return (
    <>
      <Template>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <FullCalendar
                plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                headerToolbar={{
                  left: "prev,next today",
                  center: "title",
                  right: "dayGridMonth,timeGridWeek,timeGridDay",
                }}
                initialView="dayGridMonth"
                events={eventusers}
                // editable={true}
                // selectable={true}
                // selectMirror={true}
                // dayMaxEvents={true}
                // droppable={true}
                //select={handleSelect}
                // drop={handleRecieve}
                // eventChange={handleChange}
                eventClick={handleClick}
              />
            </div>
          </div>
        </div>

        <Modal
          title="ขอแลกวันปฏิบัติงาน"
          open={isModalOpenDetail}
          // onOk={handleOk}
          //onCancel={handleCancel}
          footer={
            [
              // <Button key="submit" type="primary" onClick={handleCancelDetail}>
              //   OK
              // </Button>,
            ]
          }
        >
          {eventuserdetail.length > 0
            ? eventuserdetail.map((item) => (
                <div key={item.id}>
                  กิจกรรม : {item.Event.name} <br />
                  วันที่ : {moment(item.start).format("DD-MM-YYYY")}
                  <br />
                </div>
              ))
            : ""}
          มอบหมายให้ :
          <select
            id="receiveuser"
            onChange={(e) =>
              setChangEventUser({
                ...changeventuser,
                ReceiveUserId: e.target.value,
                fname: e.target.options[e.target.selectedIndex].getAttribute("fname"),
                lname: e.target.options[e.target.selectedIndex].getAttribute("lname")
              })
            }
            className="form-control"
          >
            <option>กรุณาเลือก</option>
            {users.length > 0
              ? users.map((item) => (
                  <option value={item.id} fname={item.fname} lname={item.lname}>
                    {item.fname} {item.lname}
                  </option>
                ))
              : ""}
          </select>
          <br />
          <Button key="DELETE" onClick={() => handleCancelDetail()}>
            ปิด
          </Button>
          <Button
            key="insert"
            onClick={() => handleSave(eventuserdetail, changeventuser,datalatest)}
          >
            บันทึก
          </Button>
        </Modal>
      </Template>
    </>
  );
}

export default ChangEventUser;
