import React, { useState, useEffect } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
//import interactionPlugin,{ Draggable } from "@fullcalendar/interaction"; // needed for dayClick
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin, { Draggable } from "@fullcalendar/interaction";
import Template from "../components/Template";
import { Button, Modal } from "antd";
import Swal from "sweetalert2";
import config from "../config";
import axios from "axios";
import Item from "antd/es/list/Item";
import moment from "moment";

function EventUser() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpenDetail, setIsModalOpenDetail] = useState(false);
  const [eventuser, setEventUser] = useState({
    title: "",
    start: "",
    end: "",
    //color:"",
    // allDay: "true",
    status: "1",
  });

  const [eventusers, setEventUsers] = useState([]);
  const [events, setEvents] = useState([]);
  const [users, setUsers] = useState([]);
  const [eventuserdetail, setEventUserDetail] = useState([]);

  useEffect(() => {
    fetchData();
    fetchDataEvent();
    fetchDataUser();
    drag();
    //fetchDataEventUserDetail()
  }, []);

  const fetchData = async () => {
    //ฟังชั่นดึงตาราง eventuser

    try {
      await axios
        .get(config.api_path + "/eventuser/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEventUsers(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataEventUserDetail = async (eventuser) => {
    //ฟังชั่นดึงตาราง eventuser
    console.log(eventuser);
    try {
      await axios
        .get(
          config.api_path + "/eventuser/userdetail",
          +eventuser.id,
          config.headers()
        )
        .then((res) => {
          if (res.data.message === "success") {
            setEventUserDetail(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataUser = async () => {
    try {
      await axios
        .get(config.api_path + "/user/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setUsers(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataEvent = async () => {
    try {
      await axios
        .get(config.api_path + "/event/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEvents(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleDateClick = (arg) => {
    alert(arg.dateStr);
  };

  const handleSelect = (info) => {
    //console.log(info);
    showModal();
    setEventUser({
      ...eventuser,
      start: info.startStr,
      end: info.endStr,
      // allday:info.allDay
    });
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    console.log(eventuser);
    setIsModalOpen(false);
  };

  const handleRecieve = (eventinfo) => {
    //ฟังชั่นบันทึกปฏิทินลากลง DB
    if (!eventuser.eventId || !eventuser.color) {
      Swal.fire({
          title: "Error",
          text: "กรุณาเลือกกิจกรรม",
          icon: "error",
      });
      return;
  }
    const addeventuser = {
      eventId: eventuser.eventId,
      color: eventuser.color,
      userId: eventinfo.draggedEl.getAttribute("userId"),
      title: eventinfo.draggedEl.getAttribute("title"),
      //color: eventinfo.draggedEl.getAttribute("color"),
      start: eventinfo.dateStr,
      end: moment(eventinfo.dateStr).add(+1, "days").format("YYYY-MM-DD"),
      status: "1",
    };
    // console.log("eventuser", addeventuser);

    handleRecieveSave(addeventuser);
    fetchData();
  };

  const handleChange = (info) => {
    //ฟังชั่นบันทึกเปลี่ยนปฏิทิน
    // console.log(info.event.startStr ,info.event.endStr, info.event._def.publicId);
    // console.log(info);

    const eventuser = {
      id: info.event._def.publicId,
      start: info.event.startStr,
      end: info.event.endStr,
    };
    // console.log("eventuser", eventuser);
    handleRecieveSave(eventuser);
  };

  const handleCancel = () => {
    setEventUser({ ...eventuser, title: "" });
    setIsModalOpen(false);
  };

  const drag = () => {
    //ฟังชั่นลากวาง

    let draggableEl = document.getElementById("external-events");
    new Draggable(draggableEl, {
      itemSelector: ".fc-event",
      eventData: function (eventEl) {
        let id = eventEl.getAttribute("id");
        let title = eventEl.getAttribute("title");
        let color = eventEl.getAttribute("color");
        //let eventId = eventEl.getAttribute("eventId");

        return {
          id: id,
          title: title,
          color: color,
          //eventId: eventId,
          create: true,
        };
      },
    });
  };

  const handleRecieveSave = async (eventuser) => {
    //ฟังชั่นบันทึกปฏิทินลง DATABASE
    try {
      let url = "/eventuser/insert";

      if (eventuser.id !== undefined) {
        url = "/eventuser/edit";
      }

      await axios.post(config.api_path + url, eventuser, config.headers());
      fetchData();
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleSave = async () => {
    //ฟังชั่นบันทึกปฏิทินลง DATABASE

    try {
      let url = "/eventuser/insert";

      if (eventuser.id !== undefined) {
        url = "/eventuser/edit";
      }

      await axios
        .post(config.api_path + url, eventuser, config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            Swal.fire({
              title: "บันทึกข้อมูล",
              text: "บันทึกข้อมูลเข้าระบบแล้ว",
              icon: "success",
              timer: 2000,
            });
            
            setEventUser({ ...eventuser, title: "" });
            setIsModalOpen(false);
            fetchData();
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleClick = async (info) => {
    //คลิกดูรายละเอียดในปฏิทิน

    const eventuser = {
      id: info.event._def.publicId,
    };
    //console.log(eventuser.id)
    try {
      await axios
        .get(
          config.api_path + "/eventuser/userdetail/" + eventuser.id,
          config.headers()
        )
        .then((res) => {
          if (res.data.message === "success") {
            setEventUserDetail(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }

    showModalDetail();
  };

  const showModalDetail = () => {
    //console.log(info)
    setIsModalOpenDetail(true);
  };

  const handleCancelDetail = () => {
    setIsModalOpenDetail(false);
  };

  const handleDeleteDetail = (item) => {
    try {
      Swal.fire({
        title: "ยืนยันการลบข้อมูล",
        text: "คุณต้องการลบข้อมูล ผู้ใช้งานใช่หรือไม่",
        icon: "question",
        showCancelButton: true,
        showConfirmButton: true,
      }).then(async (res) => {
        if (res.isConfirmed) {
          await axios
            .delete(
              config.api_path + "/eventuser/delete/" + item.id,
              config.headers()
            )
            .then((res) => {
              if (res.data.message === "success") {
                Swal.fire({
                  title: "ลบข้อมูลแล้ว",
                  text: "ระบบได้ทำการลบข้อมูลเรียบร้อยแล้ว",
                  icon: "success",
                  timer: 2000,
                });

                fetchData();
              }
            })
            .catch((err) => {
              throw err.response.data;
            });
        }
      });

      handleCancelDetail();
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }

    console.log(item.id);
  };

  return (
    <>
      <Template>
        <div className="container">
          <div className="row">
            <div className="col-2">
              <div className="card-text">
                <div id="external-events">
                  <select
                    id="eventId"
                    onChange={(e) =>
                      setEventUser({
                        ...eventuser,
                        eventId: e.target.value,
                        color:
                          e.target.options[e.target.selectedIndex].getAttribute(
                            "color"
                          ),
                      })
                    }
                  >
                    <option value={""}>กรุณาเลือกกิจกรรม</option>
                    {events.length > 0
                      ? events.map((item) => (
                          <option
                            className="fc-event fc-h-event mb-1 fc-daygrid-event fc-daygrid-block-event p-2"
                            value={item.id}
                            color={item.color}
                            style={{ backgroundColor: item.color }}
                          >
                            {item.name}
                          </option>
                        ))
                      : ""}
                  </select>
                  <div>
                    <ul>
                      {users.map((item, idex) => (
                        <li
                          className="fc-event fc-h-event mb-1 fc-daygrid-event fc-daygrid-block-event p-2"
                          userId={item.id}
                          title={`${item.fname} ${item.lname}`}
                          key={idex}
                          style={{ backgroundColor: item.color }}
                        >
                          {item.fname}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-10">
              <FullCalendar
                plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                headerToolbar={{
                  left: "prev,next today",
                  center: "title",
                  right: "dayGridMonth,timeGridWeek,timeGridDay",
                }}
                initialView="dayGridMonth"
                events={eventusers}
                editable={true}
                selectable={true}
                selectMirror={true}
                dayMaxEvents={true}
                droppable={true}
                select={handleSelect}
                drop={handleRecieve}
                eventChange={handleChange}
                eventClick={handleClick}
              />
            </div>
          </div>
        </div>

        <Modal
          title="Basic Modal"
          open={isModalOpen}
          onOk={handleSave}
          onCancel={handleCancel}
        >
          <input
            name="title"
            value={eventuser.title}
            onChange={(e) =>
              setEventUser({ ...eventuser, title: e.target.value })
            }
          />
          <select
            name="color"
            onChange={(e) =>
              setEventUser({ ...eventuser, color: e.target.value })
            }
          >
            <option key={999} value="">
              เลือกแผนก
            </option>
            {events.map((item, idex) => (
              <option
                key={idex}
                value={item.color}
                style={{ backgroundColor: item.color }}
              >
                {item.name}
              </option>
            ))}
          </select>
        </Modal>

        <Modal
          title="รายละเอียด"
          open={isModalOpenDetail}
          //onOk={handleOk}
          //onCancel={handleCancel}
          footer={
            [
              // <Button key="submit" type="primary" onClick={handleCancelDetail}>
              //   OK
              // </Button>,
            ]
          }
        >
          {eventuserdetail.length > 0
            ? eventuserdetail.map((item) => (
                <div key={item.id}>
                  กิจกรรม : {item.Event.name} <br />
                  ชื่อ : {item.user.fname} {item.user.lname}
                  <br />
                  วันที่ : {moment(item.start).format("DD-MM-YYYY")}
                  <br />
                  <br />
                  <Button key="DELETE" onClick={() => handleDeleteDetail(item)}>
                    DELETE
                  </Button>
                  <Button key="DELETE" onClick={() => handleCancelDetail()}>
                    ปิด
                  </Button>
                </div>
              ))
            : ""}
        </Modal>
      </Template>
    </>
  );
}

export default EventUser;
