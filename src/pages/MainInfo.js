import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import Template from "../components/Template";
import Swal from "sweetalert2";
import config from "../config";
import axios from "axios";

function MainInfo() {
  const [maininfo, setMainInfo] = useState({});
  const [maininfos, setMainInfos] = useState([]);
//   const [password, setPassword] = useState("");
//   const [passwordConfirm, setPasswordConfirm] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      await axios
        .get(config.api_path + "/maininfo/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setMainInfos(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });

        //console.log(maininfos)
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleSave = async () => {
    try {
      let url = "/maininfo/insert";

      if (maininfo.id !== undefined) {
        url = "/maininfo/edit";
      }

      await axios
        .post(config.api_path + url, maininfo, config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            Swal.fire({
              title: "บันทึกข้อมูล",
              text: "บันทึกข้อมูลเข้าระบบแล้ว",
              icon: "success",
              timer: 2000,
            });

            handleClose();
            fetchData();
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleClose = () => {
    const btns = document.getElementsByClassName("btnClose");
    for (let i = 0; i < btns.length; i++) {
      btns[i].click();
    }
  };

  const clearForm = () => {
    setMainInfo({
      id: undefined,
      name: "",
      status: "1",
    });
  };

  const handleDelete = (item) => {
    try {
      Swal.fire({
        title: "ยืนยันการลบข้อมูล",
        text: "คุณต้องการลบข้อมูล ผู้ใช้งานใช่หรือไม่",
        icon: "question",
        showCancelButton: true,
        showConfirmButton: true,
      }).then(async (res) => {
        if (res.isConfirmed) {
          await axios
            .delete(
              config.api_path + "/maininfo/delete/" + item.id,
              config.headers()
            )
            .then((res) => {
              if (res.data.message === "success") {
                Swal.fire({
                  title: "ลบข้อมูลแล้ว",
                  text: "ระบบได้ทำการลบข้อมูลเรียบร้อยแล้ว",
                  icon: "success",
                  timer: 2000,
                });

                fetchData();
              }
            })
            .catch((err) => {
              throw err.response.data;
            });
        }
      });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  return (
    <>
      <Template>
        <div className="card">
          <div className="card-header">
            <div className="card-title">รายละเอียดหย่วยงาน</div>
          </div>
          <div className="card-body">
            <button
              onClick={clearForm}
              data-toggle="modal"
              data-target="#modalMainInfo"
              className="btn btn-primary"
            >
              <i className="fa fa-plus me-2"></i>
              เพิ่มรายการ
            </button>

            <table className="mt-3 table table-bordered table-striped">
              <thead>
                <tr>
                  <th>หน่วยงาน</th>
                  <th>ที่อยู่</th>
                  <th>เบอร์โทรศัพท์</th>
                  <th>เลขหนังสือ</th>
                  <th>lineToken</th>
                  <th width="150px"></th>
                </tr>
              </thead>
              <tbody>
                {maininfos.length > 0
                  ? maininfos.map((item) => (
                      <tr>
                        <td>{item.name}</td>
                        <td>{item.address}</td>
                        <td>{item.phone}</td>
                        <td>{item.bookID}</td>
                        <td>{item.lineToken}</td>
                        
                        <td className="text-center">
                          <button
                            onClick={(e) => setMainInfo(item)}
                            data-toggle="modal"
                            data-target="#modalMainInfo"
                            className="btn btn-info me-2"
                          >
                            <i className="fa fa-pencil"></i>
                          </button>
                          <button
                            onClick={(e) => handleDelete(item)}
                            className="btn btn-danger"
                          >
                            <i className="fa fa-times"></i>
                          </button>
                        </td>
                      </tr>
                    ))
                  : ""}
              </tbody>
            </table>
          </div>
        </div>
      </Template>

      <Modal id="modalMainInfo" title="คำนำหน้าชื่อ" modalSize="modal-lg">
        <div className="mt-3">
          <label>ชื่อหน่วยงาน</label>
          <input
            value={maininfos.name}
            onChange={(e) => setMainInfo({ ...maininfo, name: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3">
        <label>ที่อยู่</label>
        <input
            value={maininfos.address}
            onChange={(e) => setMainInfo({ ...maininfo, address: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3">
        <label>เบอร์โทรศัพท์</label>
        <input
            value={maininfos.phone}
            onChange={(e) => setMainInfo({ ...maininfo, phone: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3"> 
        <label>เลขหนังสือ</label>
        <input
            value={maininfos.bookID}
            onChange={(e) => setMainInfo({ ...maininfo, bookID: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3">
        <label>lineToken</label>
        <input
            value={maininfos.lineToken}
            onChange={(e) => setMainInfo({ ...maininfo, lineToken: e.target.value })}
            className="form-control"
          />
        </div>
        
        <div className="mt-3">
          <button onClick={handleSave} className="btn btn-primary">
            <i className="fa fa-check me-2"></i>
            Save
          </button>
        </div>
      </Modal>
    </>
  );
}

export default MainInfo;
