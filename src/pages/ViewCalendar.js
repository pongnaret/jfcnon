import React, { useState, useEffect } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
//import interactionPlugin,{ Draggable } from "@fullcalendar/interaction"; // needed for dayClick
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin, { Draggable } from "@fullcalendar/interaction";
import Template from "../components/Template";
import { Button, Modal } from "antd";
import Swal from "sweetalert2";
import config from "../config";
import axios from "axios";
import Item from "antd/es/list/Item";
import moment from "moment";

function ViewCalendar() {
  const [isModalOpenDetail, setIsModalOpenDetail] = useState(false);

  const [eventusers, setEventUsers] = useState([]);
  const [events, setEvents] = useState([]);
  const [eventuserdetail, setEventUserDetail] = useState([]);

  useEffect(() => {
    fetchData();
    fetchDataEvent();
  }, []);

  const fetchData = async () => {
    try {
      await axios
        .get(config.api_path + "/eventuser/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEventUsers(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataEvent = async () => {
    try {
      await axios
        .get(config.api_path + "/event/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEvents(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleClick = async (info) => {
    //คลิกดูรายละเอียดในปฏิทิน

    const eventuser = {
      id: info.event._def.publicId,
    };
    //console.log(eventuser.id)
    try {
      await axios
        .get(
          config.api_path + "/eventuser/userdetail/" + eventuser.id,
          config.headers()
        )
        .then((res) => {
          if (res.data.message === "success") {
            setEventUserDetail(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }

    showModalDetail();
  };

  const showModalDetail = () => {
    //console.log(info)
    setIsModalOpenDetail(true);
  };

  const handleCancelDetail = () => {
    setIsModalOpenDetail(false);
  };

  return (
    <>
      <Template>
        <div className="container">
          <div className="row">
            {events.length > 0
              ? events.map((item) => (
                  <div
                    className="fc-event fc-h-event mb-1 fc-daygrid-event fc-daygrid-block-event p-2"
                    value={item.id}
                    color={item.color}
                    style={{ backgroundColor: item.color }}
                  >
                    {item.name}
                    <br />
                  </div>
                ))
              : ""}
          </div>
          <div className="row">
            <div className="col-12">
              <FullCalendar
                plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                headerToolbar={{
                  left: "prev,next today",
                  center: "title",
                  right: "dayGridMonth,timeGridWeek,timeGridDay",
                }}
                initialView="dayGridMonth"
                events={eventusers}
                // editable={true}
                // selectable={true}
                // selectMirror={true}
                // dayMaxEvents={true}
                // droppable={true}
                //select={handleSelect}
                // drop={handleRecieve}
                // eventChange={handleChange}
                eventClick={handleClick}
              />
            </div>
          </div>
        </div>

        <Modal
          title="รายละเอียด"
          open={isModalOpenDetail}
          //onOk={handleOk}
          //onCancel={handleCancel}
          footer={
            [
              // <Button key="submit" type="primary" onClick={handleCancelDetail}>
              //   OK
              // </Button>,
            ]
          }
        >
          {eventuserdetail.length > 0
            ? eventuserdetail.map((item) => (
                <div key={item.id}>
                  กิจกรรม : {item.Event.name} <br />
                  ชื่อ : {item.user.fname} {item.user.lname}
                  <br />
                  วันที่ : {moment(item.start).format("DD-MM-YYYY")}
                  <br />
                  <br />
                  
                  <Button key="DELETE" onClick={() => handleCancelDetail()}>
                    ปิด
                  </Button>
                </div>
              ))
            : ""}
        </Modal>
      </Template>
    </>
  );
}

export default ViewCalendar;
