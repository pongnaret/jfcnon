import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import Template from "../components/Template";
import Swal from "sweetalert2";
import config from "../config";
import axios from "axios";

function Event() {
  const [event, setEvent] = useState({});
  const [events, setEvents] = useState([]);
//   const [password, setPassword] = useState("");
//   const [passwordConfirm, setPasswordConfirm] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      await axios
        .get(config.api_path + "/event/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEvents(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleSave = async () => {
    try {
      let url = "/event/insert";

      if (event.id !== undefined) {
        url = "/event/edit";
      }

      await axios
        .post(config.api_path + url, event, config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            Swal.fire({
              title: "บันทึกข้อมูล",
              text: "บันทึกข้อมูลเข้าระบบแล้ว",
              icon: "success",
              timer: 2000,
            });

            handleClose();
            fetchData();
            
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleClose = () => {
    const btns = document.getElementsByClassName("btnClose");
    for (let i = 0; i < btns.length; i++) {
      btns[i].click();
    }
  };

  const clearForm = () => {
    setEvent({
      id: undefined,
      name: "",
      type: "1",
      color: "",
      detail: "",
      status: "1",
    });
  };

  const handleDelete = (item) => {
    try {
      Swal.fire({
        title: "ยืนยันการลบข้อมูล",
        text: "คุณต้องการลบข้อมูล ผู้ใช้งานใช่หรือไม่",
        icon: "question",
        showCancelButton: true,
        showConfirmButton: true,
      }).then(async (res) => {
        if (res.isConfirmed) {
          await axios
            .delete(
              config.api_path + "/event/delete/" + item.id,
              config.headers()
            )
            .then((res) => {
              if (res.data.message === "success") {
                Swal.fire({
                  title: "ลบข้อมูลแล้ว",
                  text: "ระบบได้ทำการลบข้อมูลเรียบร้อยแล้ว",
                  icon: "success",
                  timer: 2000,
                });

                fetchData();
              }
            })
            .catch((err) => {
              throw err.response.data;
            });
        }
      });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  return (
    <>
      <Template>
        <div className="card">
          <div className="card-header">
            <div className="card-title">กิจกรรม</div>
          </div>
          <div className="card-body">
            <button
              onClick={clearForm}
              data-toggle="modal"
              data-target="#modalevent"
              className="btn btn-primary"
            >
              <i className="fa fa-plus me-2"></i>
              เพิ่มรายการ
            </button>

            <table className="mt-3 table table-bordered table-striped">
              <thead>
                <tr>
                  <th>กิจกรรม</th>
                  <th>ประเภท</th>
                  <th>สี</th>
                  <th>รายละเอียด</th>
                  <th>สถานะ</th>
                  <th width="150px"></th>
                </tr>
              </thead>
              <tbody>
                {events.length > 0
                  ? events.map((item) => (
                      <tr>
                        <td>{item.name}</td>
                        <td>{item.type}</td>
                        <td>{item.color}</td>
                        <td>{item.detail}</td>
                        <td>
                          {(() => {
                            console.log("item.status:", item.status);
                            if (item.status === 1) {
                              return <span>ใช้</span>;
                            }  else {
                              return <span>ไม่ใช้</span>;
                            }
                          })()}
                        </td>
                        <td className="text-center">
                          <button
                            onClick={(e) => setEvent(item)}
                            data-toggle="modal"
                            data-target="#modalevent"
                            className="btn btn-info me-2"
                          >
                            <i className="fa fa-pencil"></i>
                          </button>
                          <button
                            onClick={(e) => handleDelete(item)}
                            className="btn btn-danger"
                          >
                            <i className="fa fa-times"></i>
                          </button>
                        </td>
                      </tr>
                    ))
                  : ""}
              </tbody>
            </table>
          </div>
        </div>
      </Template>

      <Modal id="modalevent" title="กิจกรรม" modalSize="modal-lg">
        <div>
          <label>กิจกรรม</label>
          <input
            value={event.name}
            onChange={(e) => setEvent({ ...event, name: e.target.value })}
            className="form-control"
          />
        </div>
        <div>
          <label>ประเภท</label>
            <select
            value={event.type}
            onChange={(e) => setEvent({ ...event, type: e.target.value })}
            className="form-control"
          >            
                  <option value="1">กิจกรรมประจำ</option>
                  <option value="2">กิจกรรมเสริม</option>

          </select>
        </div>
        <div>
          <label>รหัสสี</label>
          <input
            value={event.color}
            onChange={(e) => setEvent({ ...event, color: e.target.value })}
            className="form-control"
          />
        </div>
        <div>
          <label>รายละเอียด</label>
          <input
            value={event.detail}
            onChange={(e) => setEvent({ ...event, detail: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3">
          <label>สถานะการใช้งาน</label>
          <select
            value={event.status}
            onChange={(e) => setEvent({ ...event, status: e.target.value })}
            className="form-control"
          >
            <option value="1">ใช้งาน</option>
            <option value="0">ไม่ได้ใช้งาน</option>
          </select>
        </div>
        <div className="mt-3">
          <button onClick={handleSave} className="btn btn-primary">
            <i className="fa fa-check me-2"></i>
            Save
          </button>
        </div>
      </Modal>
    </>
  );
}

export default Event;
