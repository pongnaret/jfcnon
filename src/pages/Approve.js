import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import Template from "../components/Template";
import Swal from "sweetalert2";
import config from "../config";
import axios from "axios";

function Approve() {


  function formatDate(dateString) {
    // สร้างวัตถุ Date จากค่าวันที่ในรูปแบบของสตริง
    const date = new Date(dateString);
    // ใช้ toLocaleDateString() เพื่อแปลงวันที่ให้อยู่ในรูปแบบของภาษาท้องถิ่น
    const options = { year: "numeric", month: "long", day: "numeric" };
    return date.toLocaleDateString("th-TH", options);
  }


  const [user, setUser] = useState({});
  const [users, setUsers] = useState([]);
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [prefix, setPrefix] = useState({});
  const [prefixs, setPrefixs] = useState([]);
  const [position, setPosition] = useState({});
  const [positions, setPositions] = useState([]);

  const [approves, setApprovess] = useState([]);
  const [success, setSuccess] = useState([]);

  useEffect(() => {
    fetchData();
    fetchDataApproveSuccess();
    // fetchDataPrefix();
    // fetchDataPosition();
  }, []);

  const fetchData = async () => {
    try {
      await axios
        .get(config.api_path + "/approve/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setApprovess(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });

      console.log(approves);
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataApproveSuccess = async () => {
    try {
      await axios
        .get(config.api_path + "/approve/successlist", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setSuccess(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });

      //console.log(approves);
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleApprove = async (item) => {
    
    console.log(item);

    Swal.fire({
      title: "คุณต้องการอนุมัติหรือไม่?",
      icon: "question",
      showCancelButton: true,
      confirmButtonText: "อนุมัติ",
      cancelButtonText: "ยกเลิก",
    }).then(async (result) => {
      
      
      const approve1 = {
        id: item.id,
        approve: 1,
      };
      

      if (result.isConfirmed) {
        
        try {
          let url = "/approve/edit";
          const res = await axios.post(
            config.api_path + url,
            approve1,
            config.headers()
          );
          
          if (res.data.message === "success") {
            Swal.fire({
              title: "อนุมัติเรียบร้อยแล้ว",
              icon: "success",
              timer: 2000,
              showConfirmButton: false,
            });
    //         //สามารถเรียก fetchData() เพื่อโหลดข้อมูลใหม่หลังจากอนุมัติเสร็จสิ้น
            handleAddChangEventUser(item);
            fetchData();
            fetchDataApproveSuccess();
            
          }
        } catch (e) {
          Swal.fire({
            title: "เกิดข้อผิดพลาด",
            text: e.response.data.message,
            icon: "error",
          });
        }
      }
    });
  };

  const handleAddChangEventUser = async(item) => {
    
    const approve2 = {
       changID: item.id,
       status: 1,
       title: item.ReceiveName,
       color: item.Event.color,      
    };
    console.log(approve2)
    try {
      let url = "/eventuser/update";

      await axios.post(config.api_path + url, approve2, config.headers());      
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  
  };

  return (
    <>
      <Template>
        <div className="card">
          <div className="card-header">
            <div className="card-title">รายการรออนุมัติ</div>
          </div>
          <div className="card-body">
            <table className="mt-3 table table-bordered table-striped">
              <thead>
                <tr>
                  <th>วันที่ทำรายการ</th>
                  <th>กิจกรรม</th>
                  <th>วันที่ต้องการเปลี่ยน</th>
                  <th>ผู้ยกให้</th>
                  <th>ผู้มาแทน</th>
                  <th>สถานะ</th>
                </tr>
              </thead>
              <tbody>
                {approves.length > 0
                  ? approves.map((item) => (
                      <tr key={item.id}>
                        <td>{formatDate(item.dateChang)}</td>
                        <td>{item.Event.name}</td>
                        <td>{formatDate(item.start)}</td>
                        <td>{item.giveName}</td>
                        <td>{item.ReceiveName}</td>
                        <td>
                          <button
                            onClick={(e) => handleApprove(item)}
                            className="btn btn-danger"
                          >
                            <i>รออนุมัติ</i>
                          </button>
                        </td>
                      </tr>
                    ))
                  : ""}
              </tbody>
            </table>
          </div>
        </div>


        <div className="card">
          <div className="card-header">
            <div className="card-title">รายการที่อนุมัติแล้ว</div>
          </div>
          <div className="card-body">
            <table className="mt-3 table table-bordered table-striped">
              <thead>
                <tr>
                  <th>วันที่ทำรายการ</th>
                  <th>กิจกรรม</th>
                  <th>วันที่ต้องการเปลี่ยน</th>
                  <th>ผู้ยกให้</th>
                  <th>ผู้มาแทน</th>
                  <th>สถานะ</th>
                </tr>
              </thead>
              <tbody>
                {success.length > 0
                  ? success.map((item) => (
                      <tr>
                        <td>{formatDate(item.dateChang)}</td>
                        <td>{item.Event.name}</td>
                        <td>{formatDate(item.start)}</td>
                        <td>{item.giveName}</td>
                        <td>{item.ReceiveName}</td>
                        <td>
                          <span className="badge bg-success">อนุมัติแล้ว</span>
                        </td>
                      </tr>
                    ))
                  : ""}
              </tbody>
            </table>
          </div>
        </div>
      </Template>
    </>
  );
}

export default Approve;
