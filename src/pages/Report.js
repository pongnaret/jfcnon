import React, { useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { Form } from "react-bootstrap";
import DatePicker from "react-datepicker";
import Template from "../components/Template";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";
import Swal from "sweetalert2";
import config from "../config";

// import * as React from 'react';
import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";

function Report() {
  function formatDate(dateString) {
    // สร้างวัตถุ Date จากค่าวันที่ในรูปแบบของสตริง
    const date = new Date(dateString);
    // ใช้ toLocaleDateString() เพื่อแปลงวันที่ให้อยู่ในรูปแบบของภาษาท้องถิ่น
    const options = { year: "numeric", month: "long", day: "numeric" };
    return date.toLocaleDateString("th-TH", options);
  }

  const [report, setReport] = useState({});
  const [reports, setReports] = useState([]);

  const [events, setEvents] = useState([]);
  const [Event, setEvent] = useState(0);

  const [eventusers, setEventUsers] = useState([]);

  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  // const [reportData, setReportData] = useState([]);

  useEffect(() => {
    fetchData();
    fetchDataEvent();
    // fetchDataUser();
    // fetchDatalatest();
  }, []);

  const fetchData = async () => {
    //ฟังชั่นดึงตาราง eventuser

    try {
      await axios
        .get(config.api_path + "/report/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEventUsers(res.data.results);
          }
        })

        .catch((err) => {
          throw err.response.data;
        });
      console.log(eventusers);
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const fetchDataEvent = async () => {
    try {
      await axios
        .get(config.api_path + "/event/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEvents(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  // };

  const handleStartDateChange = (date) => {
    setStartDate(date);
  };

  const handleEndDateChange = (date) => {
    setEndDate(date);
  };

  const handleEventChange = (event) => {

    setEvent(event.target.value);
    
  };

  const handleSubmit = async () => {
    try {
      const path =
        config.api_path + "/report/dataByDate/" + startDate + "/" + endDate+ "/" + Event;
      await axios
        .get(path, config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setEventUsers(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
        console.log(Event)
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  return (
    <>
      <Template>
        <div className="card">
          <div className="card-header">
            <div className="card-title">รายงาน</div>
          </div>
          <div className="card-body">
            <div>
              <div>
                <select id="eventId" onChange={handleEventChange}>
                  <option value={0}>กิจกรรมทั้งหมด</option>
                  {events.length > 0
                    ? events.map((item) => (
                        <option
                          key={item.id}
                          value={item.id}

                        >
                          {item.name}
                        </option>
                      ))
                    : ""}
                </select>
              </div>
              <div>กรุณาเลือกช่วงเวลาที่ต้องการออกรายงาน</div>
              <div className="form-group">
                <label>Start Date: </label>
                <DatePicker
                  selected={startDate}
                  onChange={handleStartDateChange}
                  dateFormat="yyyy-MM-dd"
                />

                <label>End Date: </label>
                <DatePicker
                  selected={endDate}
                  onChange={handleEndDateChange}
                  selectsEnd
                  dateFormat="yyyy-MM-dd"
                />
              </div>

              <div className="form-group"></div>
              <button className="btn btn-primary" onClick={handleSubmit}>
                Generate Report
              </button>
            </div>
          </div>
        </div>
        <div className="card">
          <div className="card-header">
            <div className="card-title">รายงาน</div>
          </div>
          <div className="card-body">
            <Table className="mt-3 table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ลำดับ</th>
                  <th>วันที่</th>
                  <th>กิจกรรม</th>
                  <th>ชื่อ-นามสกุล</th>
                </tr>
              </thead>
              <tbody>
                {eventusers.length > 0
                  ? eventusers.map((item, index) => (
                      <tr key={item.id}>
                        <td>{index + 1}</td>
                        <td>{formatDate(item.start)}</td>
                        <td>{item.Event ? item.Event.name : "N/A"}</td>
                        <td>
                          {item.user ? item.user.fname : "N/A"}{" "}
                          {item.user ? item.user.lname : "N/A"}
                        </td>
                        {/* <td>{item.Event.name}</td>
                        <td>{item.user.name}</td>
                        <td>{item.giveName}</td>
                        <td>{item.ReceiveName}</td> */}
                        {/* <td>
                          <button
                            //onClick={(e) => handleApprove(item)}
                            className="btn btn-danger"
                          >
                            <i>รออนุมัติ</i>
                          </button>
                        </td> */}
                      </tr>
                    ))
                  : ""}
              </tbody>
            </Table>
          </div>
        </div>
      </Template>
    </>
  );
}

export default Report;
