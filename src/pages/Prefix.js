import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import Template from "../components/Template";
import Swal from "sweetalert2";
import config from "../config";
import axios from "axios";

function Prefix() {
  const [prefix, setPrefix] = useState({});
  const [prefixs, setPrefixs] = useState([]);
//   const [password, setPassword] = useState("");
//   const [passwordConfirm, setPasswordConfirm] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      await axios
        .get(config.api_path + "/prefix/list", config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            setPrefixs(res.data.results);
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleSave = async () => {
    try {
      let url = "/prefix/insert";

      if (prefix.id !== undefined) {
        url = "/prefix/edit";
      }

      await axios
        .post(config.api_path + url, prefix, config.headers())
        .then((res) => {
          if (res.data.message === "success") {
            Swal.fire({
              title: "บันทึกข้อมูล",
              text: "บันทึกข้อมูลเข้าระบบแล้ว",
              icon: "success",
              timer: 2000,
            });

            handleClose();
            fetchData();
          }
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  const handleClose = () => {
    const btns = document.getElementsByClassName("btnClose");
    for (let i = 0; i < btns.length; i++) {
      btns[i].click();
    }
  };

  const clearForm = () => {
    setPrefix({
      id: undefined,
      name: "",
      status: "1",
    });
  };

  const handleDelete = (item) => {
    try {
      Swal.fire({
        title: "ยืนยันการลบข้อมูล",
        text: "คุณต้องการลบข้อมูล ผู้ใช้งานใช่หรือไม่",
        icon: "question",
        showCancelButton: true,
        showConfirmButton: true,
      }).then(async (res) => {
        if (res.isConfirmed) {
          await axios
            .delete(
              config.api_path + "/prefix/delete/" + item.id,
              config.headers()
            )
            .then((res) => {
              if (res.data.message === "success") {
                Swal.fire({
                  title: "ลบข้อมูลแล้ว",
                  text: "ระบบได้ทำการลบข้อมูลเรียบร้อยแล้ว",
                  icon: "success",
                  timer: 2000,
                });

                fetchData();
              }
            })
            .catch((err) => {
              throw err.response.data;
            });
        }
      });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.message,
        icon: "error",
      });
    }
  };

  return (
    <>
      <Template>
        <div className="card">
          <div className="card-header">
            <div className="card-title">คำนำหน้าชื่อ</div>
          </div>
          <div className="card-body">
            <button
              onClick={clearForm}
              data-toggle="modal"
              data-target="#modalPrefix"
              className="btn btn-primary"
            >
              <i className="fa fa-plus me-2"></i>
              เพิ่มรายการ
            </button>

            <table className="mt-3 table table-bordered table-striped">
              <thead>
                <tr>
                  <th>คำนำหน้าชื่อ</th>
                  <th>สถานะ</th>
                  <th width="150px"></th>
                </tr>
              </thead>
              <tbody>
                {prefixs.length > 0
                  ? prefixs.map((item) => (
                      <tr>
                        <td>{item.name}</td>

                        <td>
                          {(() => {
                            console.log("item.status:", item.status);
                            if (item.status === 1) {
                              return <span>ใช้</span>;
                            }  else {
                              return <span>ไม่ใช้</span>;
                            }
                          })()}
                        </td>
                        <td className="text-center">
                          <button
                            onClick={(e) => setPrefix(item)}
                            data-toggle="modal"
                            data-target="#modalPrefix"
                            className="btn btn-info me-2"
                          >
                            <i className="fa fa-pencil"></i>
                          </button>
                          <button
                            onClick={(e) => handleDelete(item)}
                            className="btn btn-danger"
                          >
                            <i className="fa fa-times"></i>
                          </button>
                        </td>
                      </tr>
                    ))
                  : ""}
              </tbody>
            </table>
          </div>
        </div>
      </Template>

      <Modal id="modalPrefix" title="คำนำหน้าชื่อ" modalSize="modal-lg">
        <div>
          <label>คำนำหน้าชื่อ</label>
          <input
            value={prefix.fname}
            onChange={(e) => setPrefix({ ...prefix, name: e.target.value })}
            className="form-control"
          />
        </div>
        <div className="mt-3">
          <label>สถานะการใช้งาน</label>
          <select
            value={prefix.status}
            onChange={(e) => setPrefix({ ...prefix, status: e.target.value })}
            className="form-control"
          >
            <option value="1">ใช้งาน</option>
            <option value="0">ไม่ได้ใช้งาน</option>
          </select>
        </div>
        <div className="mt-3">
          <button onClick={handleSave} className="btn btn-primary">
            <i className="fa fa-check me-2"></i>
            Save
          </button>
        </div>
      </Modal>
    </>
  );
}

export default Prefix;
