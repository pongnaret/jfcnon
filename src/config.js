const config = {
    //api_path: 'http://localhost:3005',
    api_path: 'https://pleasant-pantsuit-deer.cyclic.app',
    token_name: 'pos_token',
    headers: ()=> {
        return{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('pos_token')
            }
         }        

    }
}

export default config;