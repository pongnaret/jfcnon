import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import axios from "axios";
import config from "../config";
import { Link } from "react-router-dom";
//import Modal from "../components/Modal";

function Sidebar() {
  const [memberName, setMemberName] = useState();
  const [packageName, setPackageName] = useState();

  useEffect(() => {
    fetchData();
    //fetchDataTotalBill();
  });

  const fetchData = async () => {
    try {
      axios.get(config.api_path + '/member/info', config.headers()).then(res => {
          if (res.data.message === "success") {
            setMemberName(res.data.result.fname);
            //setPackageName(res.data.result.package.name);
            // setBillAmount(res.data.result.package.bill_amount);
          }
        }).catch(err => {
          throw err.response.data;
        });
    } catch (e) {
      Swal.fire({
        title: "error",
        text: e.messsage,
        icon: "error",
      });
    }
  };

  return (
    <>
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <a href="/home" class="brand-link">
          <img
            src="dist/img/AdminLTELogo.png"
            alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3"
            style={{ opacity: 0.8 }}
          />
          <span class="brand-text font-weight-light">JFC Application</span>
        </a>
        <div class="sidebar">
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img
                src="dist/img/user2-160x160.jpg"
                class="img-circle elevation-2"
                alt="User Image"
              />
            </div>
            <div class="info text-white">
              <div>{memberName}</div>
              {/* <div>Package:{packageName}</div> */}
            </div>
          </div>

          <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
              <input
                class="form-control form-control-sidebar"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <div class="input-group-append">
                <button class="btn btn-sidebar">
                  <i class="fas fa-search fa-fw"></i>
                </button>
              </div>
            </div>
          </div>

          <nav className="mt-2">
            <ul
              className="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
             <li class="nav-item menu-open">
                <a href="#" class="nav-link ">
                  <i class="nav-icon fas fa-cogs"></i>
                  <p>
                    ตั้งค่าระบบ
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                
                <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="/maininfo" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>ข้อมูลหน่วยงาน</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/prefix" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>คำนำหน้าชื่อ</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/position" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>ตำแหน่ง</p>
                    </a>
                  </li>
                </ul>
            </li>

              <li className="nav-item">
                <Link to="/user" className="nav-link">
                  <i className="nav-icon fas fa-person"></i>
                  <p>จัดการผู้ใช้งานระบบ</p>
                </Link>
              </li>

              <li class="nav-item">
                <a href="/event" class="nav-link">
                  <i class="nav-icon fas fa-clipboard-list"></i>
                  <p>สร้างกิจกรรม </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/eventuser" class="nav-link">
                  <i class="nav-icon fas fa-calendar-plus"></i>
                  <p>จัดตารางการปฏิบัติงาน</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/viewcalendar" class="nav-link">
                  <i class="nav-icon far fa-calendar-alt"></i>
                  <p>ตารางการปฏิบัติงาน</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/changeventuser" class="nav-link">
                  <i class="nav-icon fas fa-exchange-alt"></i>
                  <p>ขอเปลี่ยนวันปฏิบัติงาน</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/approve" class="nav-link">
                  <i class="nav-icon fas fa-exchange-alt"></i>
                  <p>อนุมัติเปลี่ยนวันปฏิบัติงาน</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/report" class="nav-link">
                  <i class="nav-icon fas fa-chart-pie"></i>
                  <p>
                    รายงานการปฏิบัติงาน
                    
                  </p>
                </a>

              </li>

            </ul>
          </nav>
        </div>
      </aside>
    </>
  );
}

export default Sidebar;
