import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";

import reportWebVitals from "./reportWebVitals";

import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from "./pages/Home";
//import Calendar from './pages/Calendar';
import Login from "./pages/Login";
//import Package from './pages/Package';
import User from "./pages/User";
import Prefix from "./pages/Prefix";
import Position from "./pages/Position";
import Event from "./pages/Event";
import WorkSch from "./pages/WorkSch";
import EventUser from "./pages/EventUser";
import ViewCalendar from "./pages/ViewCalendar";
import ChangEventUser from "./pages/ChangEventUser";
import Approve from "./pages/Approve";
import Report from "./pages/Report";
import MainInfo from "./pages/MainInfo";


const router = createBrowserRouter([
  {
    path: "/home",
    element: <Home />,
  },
  {
    path: "/",
    element: <Login />,
  },
  // {
  //   path: "/calendar",
  //   element: <Calendar />
  // },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/user",
    element: <User />,
  },
  {
    path: "/prefix",
    element: <Prefix />,
  },
  {
    path: "/position",
    element: <Position />,
  },
  {
    path: "/event",
    element: <Event />,
  },
  {
    path: "/worksch",
    element: <WorkSch />,
  },
  {
    path: "/eventuser",
    element: <EventUser />,
  },
  {
    path: "/viewcalendar",
    element: <ViewCalendar />,
  },
  {
    path: "/changeventuser",
    element: <ChangEventUser />,
  },
  {
    path: "/approve",
    element: <Approve />,
  },
  {
    path: "/report",
    element: <Report />,
  },
  {
    path: "/maininfo",
    element: <MainInfo />
  }

]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<RouterProvider router={router} />);

reportWebVitals();
